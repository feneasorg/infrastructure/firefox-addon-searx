# Firefox Addon for Searx

# Dependencies

Use yarn or npm to install dependencies:

    yarn

# Development

Install and run the plugin by executing:

    yarn dev

# Packaging

Bundle all files into a single distribution archive:

    yarn package
